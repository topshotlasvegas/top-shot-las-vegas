THE MOST FUN YOU CAN HAVE WITH A FIREARM.
Experience the thrills of bold firepower, heart-pounding excitement, and action-packed fun.  Top Shot Las Vegas combines the authentic shooting experience of a gun range with the fun of first-person-shooter gaming.

Address: 3084 S Highland Dr, Ste C, Las Vegas, NV 89109, USA

Phone: 702-478-8550
